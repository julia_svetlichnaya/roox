package test.task.roox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RooxApp {

    public static void main(String[] args) {
        SpringApplication.run(RooxApp.class);
    }
}
