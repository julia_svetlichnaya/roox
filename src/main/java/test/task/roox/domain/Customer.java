package test.task.roox.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

import static test.task.roox.domain.Customer.Status.ACTIVE;

@Entity
@Getter
@Setter
public class Customer {

    public static final Status DEFAULT_STATUS = ACTIVE;
    public static final BigDecimal DEFAULT_BALANCE = BigDecimal.ZERO;

    public enum Status {
        ACTIVE, BLOCKED
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String uuid;

    @Embedded
    private FullName fullName;

    @NotNull
    private BigDecimal balance = DEFAULT_BALANCE;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = DEFAULT_STATUS;

    @Embedded
    private Credentials credentials;
}
