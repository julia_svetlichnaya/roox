package test.task.roox.domain;

import lombok.*;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FullName {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private String patronymic;
}
