package test.task.roox.domain;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Data
public class PartnerMapping {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String uuid;

    private String partnerId;

    private String customerExternalId;

    @Embedded
    private FullName customerExternalFullName;

    @ManyToOne
    @JoinColumn(nullable = false)
    private Customer customer;

    //todo add avatar
}
