package test.task.roox.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.task.roox.domain.Customer;

public interface CustomerRepository extends JpaRepository<Customer, String> {
}
