package test.task.roox.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import test.task.roox.domain.Credentials;
import test.task.roox.domain.Customer;
import test.task.roox.domain.FullName;
import test.task.roox.domain.repository.CustomerRepository;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class CustomerService {
    //todo it seems that schema is not validated during context initializing
    public final CustomerRepository customerRepository;

    public Customer create() {

        Customer customer = new Customer();
        customer.setCredentials(Credentials.builder()
                .login("safo775")
                .password("123")
                .build());
        customer.setFullName(FullName.builder()
                .firstName("Yulia")
                .lastName("Svetlichnaya")
                .build());
        return customerRepository.save(customer);
    }

    public Customer get(String uuid) {
        return customerRepository.findOne(uuid);
    }

}
