CREATE TABLE customer (
  uuid       CHAR(36) PRIMARY KEY,
  first_name VARCHAR(255) NOT NULL,
  last_name  VARCHAR(255) NOT NULL,
  patronymic VARCHAR(255),
  balance    NUMERIC      NOT NULL,
  status     VARCHAR(255) NOT NULL,
  login      VARCHAR(255) NOT NULL,
  password   CHAR(60)     NOT NULL
);

