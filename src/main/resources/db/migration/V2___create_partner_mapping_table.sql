CREATE TABLE partner_mapping (
  uuid                 CHAR(36) PRIMARY KEY,
  partnerId            VARCHAR(255) NOT NULL,
  customer_external_id VARCHAR(255) NOT NULL,
  first_name           VARCHAR(255),
  last_name            VARCHAR(255),
  patronymic           VARCHAR(255),
  customer_id          CHAR(36)     NOT NULL    CONSTRAINT fk_partner_mapping_customer REFERENCES customer
);

