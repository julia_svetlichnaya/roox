package test.task.roox.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import test.task.roox.domain.Customer;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CustomerServiceTest {

    @Autowired
    private CustomerService customerService;

    @Test
    public void should_create_customer() {
        //when
        Customer customer = customerService.create();

        //then
        Customer foundCustomer = customerService.get(customer.getUuid());
        assertThat(foundCustomer).isNotNull();
        assertThat(foundCustomer).isEqualToIgnoringGivenFields(customer, "balance");
        assertThat(foundCustomer.getBalance().doubleValue()).isEqualTo(customer.getBalance().doubleValue());
    }
}